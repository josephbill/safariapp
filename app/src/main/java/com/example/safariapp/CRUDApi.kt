package com.example.safariapp

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.activity_c_r_u_d_api.*
import org.json.JSONObject

class CRUDApi : AppCompatActivity() {
    private var imageUri: Uri? = null
    //variables to store users input on edit text
    var texta: String = ""
    var textb: String = ""
    var userID = "2"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_c_r_u_d_api)

        postData.setOnClickListener {
             postToApi()
        }
        deleteData.setOnClickListener {
            deleteUsingApi()
        }
        updateData.setOnClickListener {
            updateUsingApi()
        }

    }

    private fun deleteUsingApi() {
        var url = "https://postman-echo.com/delete?" + userID

        val request = StringRequest(
            Request.Method.DELETE, url,
            { response -> // response
                try {
                    Log.d("message", "Response: $response")
                    Toast.makeText(applicationContext,"Delete successful",Toast.LENGTH_LONG).show()

                } catch (e: Exception) {
                    Log.d("message", "Exception: $e")
                }
            }
        ) {
            // error.
            Log.d("message", "Volley error: $it")
            Toast.makeText(applicationContext,"Check internet ",Toast.LENGTH_LONG).show()


        }

        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )


        //adding request to volley
        VolleySingleton.getInstance(this).addToRequestQueue(request)

    }

    private fun updateUsingApi() {
        //capturing users input
        texta = textaa.text.toString()
        textb = textba.text.toString()
        
        //update using api
        usingApiUpdate(texta,textb,userID)
    }

    private fun usingApiUpdate(texta: String, textb: String, userID: String) {
        val url = "https://postman-echo.com/put?" + userID
        //update params
        //form fields and values
        val params = HashMap<String, String>()
        params["parameterfield"] = texta
        params["anotherparamfield"] = textb
        val jsonObject = JSONObject(params as Map<*, *>)

        val request = JsonObjectRequest(Request.Method.PUT,url,jsonObject, Response.Listener { response ->
            //capture success
            // Process the json
            try {
                Log.d("message", "Response: $response")
                Toast.makeText(applicationContext,"Update successful",Toast.LENGTH_LONG).show()
            } catch (e: Exception) {
                Log.d("message", "Exception: $e")
            }
        }, Response.ErrorListener {
            //capture failure
            Log.d("message", "Exception: $it")

            Toast.makeText(applicationContext,"Error , check internet",Toast.LENGTH_LONG).show()
        })

        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )


        //adding request to volley
        VolleySingleton.getInstance(this).addToRequestQueue(request)

    }

    fun postData(view: View){
        //using image picker to select image
        ImagePicker.with(this)
            .crop()	    			//Crop image(Optional), Check Customization for more option
            .compress(1024)			//Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            imageUri = data?.data!!

            // Use Uri object instead of File to avoid storage permissions
            image1.setImageURI(imageUri)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    private fun postToApi() {
        //capturing users input
        texta = textaa.text.toString()
        textb = textba.text.toString()
        //post to api
        submitToApi(texta,textb,imageUri)
    }

    private fun submitToApi(texta: String, textb: String, imageUri: Uri?) {
        val url = "https://postman-echo.com/post"


        // Post parameters
        // Form fields and values
        val params = HashMap<String, String>()
        params["foo1"] = texta
        params["foo2"] = textb
        val jsonObject = JSONObject(params as Map<*, *>)

        val request = JsonObjectRequest(Request.Method.POST,url,jsonObject, Response.Listener {response ->
            //capture success
            // Process the json
            try {
                Log.d("message", "Response: $response")
                Toast.makeText(applicationContext,"Submit successful",Toast.LENGTH_LONG).show()
            } catch (e: Exception) {
                Log.d("message", "Exception: $e")
            }
        }, Response.ErrorListener {
            //capture failure
            Log.d("message", "Exception: $it")

            Toast.makeText(applicationContext,"Error , check internet",Toast.LENGTH_LONG).show()

        })

        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )


        //adding request to volley
        VolleySingleton.getInstance(this).addToRequestQueue(request)



    }
}